public class Fibbonaci {
    public static void main(String[] args) {
        int fibNum1=1;
        int fibNum2=1;
        int amount=11;
        int fibSum;
        for(int i=0;i<amount;i++){
            System.out.print(fibNum1 + " ");
            fibSum = fibNum1 + fibNum2;
            fibNum1 = fibNum2;
            fibNum2 = fibSum;
        }
        System.out.println();
        fibNum1 = fibNum2 = 1;
        int i=0;
        while(i<amount){
            System.out.print(fibNum1 + " ");
            fibSum = fibNum1 + fibNum2;
            fibNum1 = fibNum2;
            fibNum2 = fibSum;
            i++;
        }
    }
}
